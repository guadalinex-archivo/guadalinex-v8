# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Serbian/Cyrillic messages for debian-installer.
# Copyright (C) 2010 Software in the Public Interest, Inc.
# Copyright (C) 2008 THE cp6Linux'S COPYRIGHT HOLDER
# This file is distributed under the same license as the debian-installer package.
# Karolina Kalic <karolina@janos.in.rs>, 2010.
# Janos Guljas <janos@janos.in.rs>, 2010.
# Veselin Mijušković <veselin.mijuskovic@gmail.com>, 2008.
# , fuzzy
#
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-btrfs@packages.debian.org\n"
"POT-Creation-Date: 2010-07-17 06:23+0000\n"
"PO-Revision-Date: 2010-07-18 01:37+0100\n"
"Last-Translator: Janos Guljas <janos@janos.in.rs>\n"
"Language-Team: Serbian/Cyrillic\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#. Type: text
#. Description
#. :sl2:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-btrfs.templates:1001 ../partman-btrfs.templates:3001
msgid "btrfs"
msgstr "btrfs"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-btrfs.templates:2001
msgid "btrfs journaling file system"
msgstr "btrfs журналски фајл систем"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid "btrfs root file system not supported without separate /boot"
msgstr ""
"Фајл систем btrfs као root фајл систем није подржан без одвојене /boot тачке "
"монтирања"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"Your root file system is a btrfs file system. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"Ваш root фајл систем је типа btrfs. Ово може узроковати проблеме са "
"подразумеваним бут лоудером."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"You should use a small /boot partition with another file system, such as "
"ext3."
msgstr ""
"Требало би да направите малу /boot партицију другог типа, као што је ext3."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid "btrfs file system not supported for /boot"
msgstr "Фајл систем btrfs није подржан за /boot"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You have mounted a btrfs file system as /boot. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"Монтирали сте btrfs фајл систем као /boot. Ово ће врло вероватно узроковати "
"проблеме са подразумеваним бут лоудером."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You should use another file system, such as ext3, for the /boot partition."
msgstr ""
"Требало би да користите други тип фајл система као што је ext3, за /boot "
"партицију."
