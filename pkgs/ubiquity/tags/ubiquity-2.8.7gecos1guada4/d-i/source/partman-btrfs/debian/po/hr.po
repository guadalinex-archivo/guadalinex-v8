# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Croatian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
msgid ""
msgstr ""
"Project-Id-Version: Debian-installer 1st-stage master file HR\n"
"Report-Msgid-Bugs-To: partman-btrfs@packages.debian.org\n"
"POT-Creation-Date: 2010-07-17 06:23+0000\n"
"PO-Revision-Date: 2010-07-07 12:30+0200\n"
"Last-Translator: Josip Rodin <joy+ditrans@linux.hr>\n"
"Language-Team: Croatian <lokalizacija@linux.hr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl2:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-btrfs.templates:1001 ../partman-btrfs.templates:3001
msgid "btrfs"
msgstr "btrfs"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-btrfs.templates:2001
msgid "btrfs journaling file system"
msgstr "btrfs dnevnički datotečni sustav"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid "btrfs root file system not supported without separate /boot"
msgstr "btrfs korijenski datotečni sustav nije podržan bez odvojenog /boot"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"Your root file system is a btrfs file system. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"Vaš korijenski datotečni sustav je btrfs. To će vjerojatno izazvati probleme "
"s boot učitavačem koji je zadan za ovu instalaciju."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"You should use a small /boot partition with another file system, such as "
"ext3."
msgstr ""
"Trebali biste rabiti malu /boot particiju s drugim datotečnim sustavom, "
"recimo ext3."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid "btrfs file system not supported for /boot"
msgstr "btrfs nije podržani datotečni sustav za /boot"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You have mounted a btrfs file system as /boot. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"Montirali ste btrfs datotečni sustav kao /boot. To nije podržano u boot "
"učitavaču koji je zadan za ovu instalaciju."

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You should use another file system, such as ext3, for the /boot partition."
msgstr ""
"Trebali biste rabiti drugi datotečni sustav, npr. ext3, za /boot particiju."
