# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Lithuanian messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
# Marius Gedminas <mgedmin@b4net.lt>, 2004.
# Darius Skilinskas <darius10@takas.lt>, 2005.
# Kęstutis Biliūnas <kebil@kaunas.init.lt>, 2004...2010.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: partman-jfs@packages.debian.org\n"
"POT-Creation-Date: 2010-02-26 22:59+0000\n"
"PO-Revision-Date: 2010-09-07 09:04+0300\n"
"Last-Translator: Kęstutis Biliūnas <kebil@kaunas.init.lt>\n"
"Language-Team: Lithuanian <komp_lt@konferencijos.lt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: lt\n"

#. Type: text
#. Description
#. :sl1:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. Short file system name (untranslatable in many languages)
#. :sl1:
#: ../partman-jfs.templates:1001 ../partman-jfs.templates:3001
msgid "jfs"
msgstr "jfs"

#. Type: text
#. Description
#. File system name
#. :sl2:
#: ../partman-jfs.templates:2001
msgid "JFS journaling file system"
msgstr "JFS žurnalinė failų sistema"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid "Use unrecommended JFS root file system?"
msgstr "Ar naudoti nerekomenduotiną JFS kaip šakninę (root) failų sistemą?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid ""
"Your root file system is a JFS file system. This can cause problems with the "
"boot loader used by default by this installer."
msgstr ""
"Jūsų šakninė (root) failų sistema yra JFS. Tai gali sukelti problemų "
"pradiniam įkėlikliui, pagal nutylėjimą naudojamam šiame įdiegiklyje."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:4001
msgid ""
"You should use a small /boot partition with another file system, such as "
"ext3."
msgstr ""
"Jūs turite naudoti nedidelį /boot skirsnį su kita failų sistema, kaip pvz. "
"ext3."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:5001
msgid "Use unrecommended JFS /boot file system?"
msgstr "Ar naudoti nerekomenduotiną JFS failų sistemą katalogui /boot?"

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:5001
msgid ""
"You have mounted a JFS file system as /boot. This is likely to cause "
"problems with the boot loader used by default by this installer."
msgstr ""
"Prijungėte JFS failų sistemą kaip /boot. Tai gali sukelti problemų pradiniam "
"įkėlikliui, pagal nutylėjimą naudojamam šiame įdiegiklyje."

#. Type: boolean
#. Description
#. :sl2:
#: ../partman-jfs.templates:5001
msgid ""
"You should use another file system, such as ext3, for the /boot partition."
msgstr "Jūs turite naudoti /boot skirsniui kitą failų sistemą, kaip pvz. ext3."
