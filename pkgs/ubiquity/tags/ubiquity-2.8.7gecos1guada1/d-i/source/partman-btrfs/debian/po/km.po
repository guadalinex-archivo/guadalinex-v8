# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of debian-installer_packages_po_sublevel1_km.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2006, 2007, 2008, 2010.
msgid ""
msgstr ""
"Project-Id-Version: debian-installer_packages_po_sublevel1_km\n"
"Report-Msgid-Bugs-To: partman-btrfs@packages.debian.org\n"
"POT-Creation-Date: 2010-07-17 06:23+0000\n"
"PO-Revision-Date: 2010-12-13 13:47+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. Type: text
#. Description
#. :sl2:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl1:
#. Short file system name (untranslatable in many languages)
#: ../partman-btrfs.templates:1001 ../partman-btrfs.templates:3001
msgid "btrfs"
msgstr "btrfs"

#. Type: text
#. Description
#. :sl2:
#. File system name
#: ../partman-btrfs.templates:2001
msgid "btrfs journaling file system"
msgstr "ប្រព័ន្ធ​ឯកសារ​ទិន្នានុប្បវត្តិ btrfs"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid "btrfs root file system not supported without separate /boot"
msgstr "ប្រព័ន្ធ​ឯកសារ btrfs root នឹង​មិន​ត្រូវ​បាន​គាំទ្រ​ដោយ​គ្មាន /boot ផ្សេង​ពី​គ្នា​ទេ"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"Your root file system is a btrfs file system. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"ប្រព័ន្ធ​ឯកសារ​ root របស់​អ្នក​គឺ​ជា​ប្រព័ន្ធ​ឯកសារ​ btrfs ។ វា​អាច​បណ្ដាល​ឲ្យ​មាន​បញ្ហា​ជាមួយ​នឹង​កម្មវិធី​"
"ចាប់ផ្ដើម​ប្រព័ន្ធ ដែល​ប្រើ​ជា​លំនាំដើម​ដោយ​កម្មវិធី​ដំឡើង​នេះ​ ។"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:4001
msgid ""
"You should use a small /boot partition with another file system, such as "
"ext3."
msgstr "អ្នក​គួរ​ប្រើ​ភាគថាស /boot តូច​មួយ​ជាមួយ​នឹង​ប្រព័ន្ធ​ឯកសារ​មួយ​ទៀត ដូចជា ext3 ជាដើម ។"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid "btrfs file system not supported for /boot"
msgstr "ប្រព័ន្ធ​ឯកសារ​ ​btrfs នឹង​មិន​ត្រូវ​បាន​គាំ​ទ្រ​សម្រាប់​ /boot ទេ"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You have mounted a btrfs file system as /boot. This is not supported by the "
"boot loader used by default by this installer."
msgstr ""
"អ្នក​បាន​ម៉ោន​ប្រព័ន្ធ​ឯកសារ​ btrfs ជា​ /boot ។ វា​នឹង​មិន​ត្រូវ​បាន​គាំ​ទ្រ​ដោយ​កម្មវិធី​ចាប់​ផ្ដើម​ប្រព័ន្ធ​តាម​"
"លំនាំ​ដើម​​ដោយ​កម្មវិធី​ដំឡើង​នេះ​ទេ ​។"

#. Type: error
#. Description
#. :sl2:
#: ../partman-btrfs.templates:5001
msgid ""
"You should use another file system, such as ext3, for the /boot partition."
msgstr "អ្នក​គួរ​ប្រើ​ប្រព័ន្ធ​ឯកសារមួយ​ទៀត ដូចជា​ ext3 ជាដើម សម្រាប់​​​ភាគថាស /boot ។"
