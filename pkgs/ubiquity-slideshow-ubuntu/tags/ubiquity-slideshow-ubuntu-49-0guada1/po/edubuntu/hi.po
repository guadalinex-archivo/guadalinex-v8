# Hindi translation for ubiquity-slideshow-ubuntu
# Copyright (c) 2011 Rosetta Contributors and Canonical Ltd 2011
# This file is distributed under the same license as the ubiquity-slideshow-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: ubiquity-slideshow-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2011-09-28 16:16+0200\n"
"PO-Revision-Date: 2011-03-03 09:38+0000\n"
"Last-Translator: Manish Kumar <manishku86@yahoo.co.in>\n"
"Language-Team: Hindi <hi@li.org>\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-10-03 09:05+0000\n"
"X-Generator: Launchpad (build 14071)\n"

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/accessibility.html:1
msgid "Accessibility in Edubuntu"
msgstr "एडुबन्टू में अभिगम्यता"

#.  "your" refers to a large group that includes the reader. 
#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/accessibility.html:6
msgid ""
"We want to make computers work for everyone, whatever your physical "
"circumstances. So, we provide tools that make Edubuntu one of the most "
"accessible operating systems around."
msgstr ""
"हम चाहते हैं कि कंप्यूटर सभी के लिए काम करे चाहे आपका भौतिक परिस्थिति जो भी हो. अतः हम "
"ऐसे औजार प्रदान करते हैं जो एडुबन्टू को उपलब्ध ऑपरेटिंग सिस्टम में सर्वाधिक अभिगम्यता वाला "
"बना दे."

#.  The string "Assistive Technologies Preferences" is describing a particular menu item: System >
#.  Preferences > Assistive Technologies, and the string "System menu" describes that section of the main menu. Note that certain words are kept the same to make the menu items discoverable. It is intentional that we are using soft descriptions instead of mapping out a rigid path for the user to follow. Please, if it is natural for your locale, consider the same convention. 
#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/accessibility.html:7
msgid ""
"You can get at these tools in one place: the <em>Assistive Technologies "
"Preferences</em>, inside the System Menu. From there, you can turn on "
"helpful tools like <em>Orca</em>, to speak text on the screen, or dwell "
"click to press mouse buttons automatically."
msgstr ""
"आप  सभी उपकरण को एक ही जगह पाते हैः तंत्र सूची के अंतर्गत <em>Assistive Technologies "
"Preferences</em> जहाँ से आप सहायकपूर्ण उपकरण जैसे  <em>Orca</em> को चालू कर सकते हैं, "
"ताकि आपके स्क्रीन पर के पाठ को सुन सके, या स्वचालित dwell क्लिक हेतु माउस के बटन को "
"दबायें."

#.  As before, the string "Appearance Preferences" is describing a particular menu item: System >
#.  Preferences > Appearance. Certain words were kept the same to make the menu item discoverable. 
#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/accessibility.html:8
msgid ""
"Remember to check out the <em>Appearance Preferences</em>, too. You can "
"choose between different visual styles and even change the fonts that are "
"used by applications."
msgstr ""
"<em>सूची वरीयताएँ</em> की जांच करना न भुले, जहां आप अनुप्रयोगों द्वारा उपयोग किये जाने "
"वाले विभिन्न दृश्य शैली तथा फ़ॉन्ट को भी बदल सकतें है."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/arkose.html:1
msgid "Arkose application sandboxing"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/arkose.html:6
msgid ""
"<em>Arkose</em> allows you to run an application in a contained environment."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/arkose.html:7
msgid ""
"You can now test downloaded software in a protected environment easily "
"before deploying."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/arkose.html:8
msgid ""
"Integrates with file manager allowing safe execution with a few simple "
"clicks."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/calc.html:1
msgid "Gnome Calculator"
msgstr "गनोम गणक"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calc.html:6
msgid ""
"Edubuntu ships with the latest version of the powerful <em>Gnome Calculator</"
"em>."
msgstr "एडुबन्टू शक्तिशाली <em>गनोम गणक</em> के नवीनतम संस्करण के साथ प्रेषित होता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calc.html:7
msgid "It supports financial operations and up-to-date currency conversions."
msgstr "यह वित्तीय क्रियाकलाप तथा अद्यतन मूद्रा परिवर्तन को समर्थित करता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calc.html:8
msgid ""
"Use the programming function for logical computations and advanced mode for "
"scientific computations."
msgstr ""
"लॉजिकल कंप्यूटेशन तथा वैज्ञानिक कंप्यूटेशन हेतु उन्नत पद्धति हेतु प्रोग्रामिंग फंक्शन का उपयोग करें."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/calibre.html:1
msgid "Calibre e-book manager"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calibre.html:6
msgid ""
"<em>Calibre</em> is an e-library solution for cataloging electronic books."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calibre.html:7
msgid ""
"It supports many formats, including MOBI, LIT, PRC, EPUB, ODT, HTML, CBR, "
"RTF, PDF and more."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calibre.html:8
msgid ""
"Supports for many different hardware platforms, including the most popular "
"commercial e-book platforms available."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/calibre.html:9
msgid "Automatically sync feeds and news to your favorite device."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/desktop-software.html:1
msgid "Great desktop software"
msgstr "उत्कृष्ट डेक्सटॉप सॉफ्टवेयर"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/desktop-software.html:6
msgid ""
"<em>LibreOffice</em> is a powerful office software suite that is easy to "
"understand."
msgstr ""
"<em>लिबरे ऑफिस</em> एक शक्तिशाली ऑफिस सॉफ्टवेयर सुईट है जिसे आसानी से समझा जा सकता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/desktop-software.html:7
msgid ""
"<em>Firefox</em> provides a safe and easy web browsing experience with many "
"useful add-ons available."
msgstr ""
"<em>फ़ायरफॉक्स</em> सुरक्षित तथा आसान संजाल ब्राउजिंग अनुभव तथा कई सारे उपयोगी "
"सहयुक्तियों के साथ उपलब्ध है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/desktop-software.html:8
msgid ""
"<em>Inkscape</em> is a vector drawing program capable of exporting to many "
"popular formats."
msgstr ""
"<em>इंकस्केप</em> एक वेक्टर ड्राइंग कार्यक्रम है जो कई लोकप्रिय फ़ार्मेट में निर्यात करने में "
"समर्थ है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/desktop-software.html:9
msgid "<em>Gimp</em> is a powerful and scriptable image and photo editor."
msgstr "<em>जिम्प</em> एक  स्क्रीप्टसमर्थ तथा शक्तिशाली छवि एंव फोटो संपादक है."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/documentation.html:1
msgid "Getting Help with Edubuntu"
msgstr "एडुबन्टू के साथ मदद प्राप्त करें"

#.  The string "Help and Support" should be exactly as it appears in the System menu. 
#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/documentation.html:6
msgid ""
"If you need help, try <em>Help and Support</em> from the System menu, or the "
"<em>Help</em> menu in most applications."
msgstr ""
"यदि आपको मदद की आवश्यकता है, तो तंत्र सूची से <em>मदद तथा समर्थन</em> का  या अधिकांश "
"अनुप्रयोग में <em>मदद</em> सूची का उपयोग करें."

#.  The url "edubuntu.org/support" should not be translated 
#. type: Content of: <div><div><ul><li><a>
#: slideshows/edubuntu/slides/documentation.html:7
msgid ""
"In addition to our extensive written help, the Edubuntu community provides "
"free technical support over the Internet. There is also commercial support "
"available through Canonical, its partners and approved companies. Learn more "
"at <a href=\"http://www.edubuntu.org/documentation\">"
msgstr ""
"हमारे विस्तृत लिखे मदद के अतिरिक्त, अंतर्जाल पर एडुबन्टू समुदाय आपको मुफ्त तकनीकी समर्थन "
"प्रदान करता है. इसके अतिरिक्त कैनोनिकल, इसके सहयोगी तथा अनुमोदित कंपनी द्वारा "
"वाणिज्यिक समर्थन भी उपलब्ध है. अधिक जानकारी के लिए देखें <a href=\"http://www."
"edubuntu.org/documentation\">"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/documentation.html:7
msgid "edubuntu.org</a>"
msgstr "edubuntu.org</a>"

#.  The url "edubuntu.org/community" should not be translated 
#. type: Content of: <div><div><ul><li><a>
#: slideshows/edubuntu/slides/documentation.html:8
msgid ""
"Let us know about your Edubuntu experience at <a href=\"http://edubuntu.com/"
"community\">"
msgstr ""
"अपने एडुबन्टू अनुभव के बारे में हमें बताएँ <a href=\"http://edubuntu.com/community\">"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/documentation.html:8
msgid "edubuntu.org/community</a>"
msgstr "edubuntu.org/community</a>"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/documentation.html:9
msgid ""
"Thanks for viewing our slideshow, if you selected additional options, then "
"the installation process will take a bit longer to complete."
msgstr ""
"हमारे स्लाईडशो को देखने के लिए धन्यवाद, यदि आप अतिरिक्त विकल्प चुनना चाहते है, तब "
"संस्थापना प्रक्रिया पूरा होने कुछ अधिक लगेगा."

#. type: Content of: <div><div><p>
#: slideshows/edubuntu/slides/documentation.html:11
msgid "The installation will finish soon. We hope you enjoy Edubuntu."
msgstr "यह संस्थापना जल्द ही समाप्त हो जाएगा. हमे आशा है की आप एडुबन्टू को आनंद लेगें."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/edubuntu-menu-editor.html:1
msgid "Edubuntu menu editor"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/edubuntu-menu-editor.html:6
msgid ""
"The <em>Edubuntu Menu Editor</em> allows administrators to create and apply "
"custom menus."
msgstr ""
"<em>एडुबन्टू सूची संपादक</em> प्रशासक को मनपसंद सूची बनाने तथा लागू करने की अनुमति देता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/edubuntu-menu-editor.html:7
msgid "Menus are created and applied using an easy to use menu editor."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/edubuntu-menu-editor.html:8
msgid "Settings can be applied to users or groups system-wide."
msgstr "विन्यास को पुरे तंत्र में प्रयोक्ता या समूह लागू होगा."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/empathy.html:1
msgid "Social from the start"
msgstr "आरंभ से ही सामाजिक"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/empathy.html:6
msgid ""
"From the start, Edubuntu connects to online chat and broadcast services "
"including Facebook, Twitter, Windows Live and Google Talk."
msgstr ""
"आरंभ से ही, एडुबन्टू ऑनलाइन दोस्त से जुड़ता है तथा फ़ेसबुक, ट्वीटर, विंडो लाइव तथा गुगल टॉक "
"सहीत सेवाओं का प्रसारण करता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/empathy.html:7
msgid ""
"Click the envelope near the clock to log in to your accounts. New messages "
"appear there too, so you can see what's happening at a glance."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/empathy.html:8
msgid ""
"If you're busy, click your name at the top of the screen to change your "
"status."
msgstr ""
"यदि आप व्यस्त है, तो स्क्रीन के उपरी छोड़ पर अपने नाम पर क्लिक कर अपनी स्थिति को बदल "
"सकते हैं."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/freemind.html:1
msgid "FreeMind mindmapping"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/freemind.html:6
msgid "<em>FreeMind</em> is a powerful mindmapping tool."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/freemind.html:7
msgid "Summarize huge amounts of data efficiently."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/freemind.html:8
msgid "Make subsections stand out with cloud bubbles and custom colours."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/freemind.html:9
msgid ""
"Organize your goals, future plans, events and more with this great tool!"
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/gbrainy.html:1
msgid "Exercise your gray matter"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/gbrainy.html:6
msgid ""
"Edubuntu ships with <em>gbrainy</em>, a platform to train memory, "
"arithmetical and logical capabilities."
msgstr ""
"एडुबन्टू <em>जीब्रेनी</em> जो एक स्मृति को प्रशिक्षित करने, अंकगणितीय तथा तार्किक क्षमता "
"हेतु प्लेटफॉर्म है, के साथ आता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/gbrainy.html:7
msgid ""
"Logic Puzzles: games designed to challenge your reasoning and thinking "
"skills."
msgstr ""
"तार्किक पहेली: इस खेल का डिजाईन आपके तर्क वितर्क को चुनौती देने तथा सोचने की क्षमता को "
"बढ़ाने के लिए किया गया है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/gbrainy.html:8
msgid ""
"Mental Calculation: games based on arithmetical operations designed to prove "
"your mental calculation skills."
msgstr ""
"मानसिक गणना: यह खेल अंकगणितीय क्रियाकलाप पर आधारित डिजाईन किया गया ताकि आपके "
"मानसिक गणना क्षमता को साबित किया जा सके."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/gbrainy.html:9
msgid "Memory Trainers: games designed to challenge your short term memory."
msgstr ""
"स्मृति प्रशिक्षक: यह खेल आपके अल्पावधि स्मृति को चुनौति देने के लिए डिजाईन किया गया है."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/geogebra.html:1
msgid "Geogebra dynamic mathematics"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/geogebra.html:6
msgid "<em>Geogebra</em> is a dynamic geometry system."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/geogebra.html:7
msgid "Do constructions with points, vectors, segments, lines and more."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/geogebra.html:8
msgid "Modify functions dynamically and edit equations directly."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/geogebra.html:9
msgid ""
"Supports many elementary calculus-based tools such as derivatives, "
"osculating circle and more."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/installing-software.html:1
msgid "Getting more software"
msgstr "और ज्यादा सॉफ्टवेयर प्राप्त करें"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/installing-software.html:6
msgid ""
"Need more software? Try the <em>Ubuntu Software Center</em> to choose from "
"thousands of extras you can download for free."
msgstr ""
"और सॉफ्टवेयर चाहिए? <em>उबुन्टू सॉफ्टवेयर केन्द्र</em> में जाकर हजारों अतरिक्त में से चुनकर आप "
"मुफ्त में डाउनलोड कर सकते हैं."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/installing-software.html:7
msgid ""
"There are powerful applications for all sorts of interests, from programming "
"to creating music and exploring the universe. Take a look at the "
"<em>Featured Applications</em> section for some of our favorites!"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/installing-software.html:8
msgid ""
"Everything in Software Center is stored in our online repository. We are "
"careful to keep it safe and updates will be delivered directly to you."
msgstr ""
"सॉफ्टवेयर केन्द्र का प्रत्येक चीज हमारे ऑनलाइन भण्डार में संगृहित है. हमलोग इस सावधानीपूर्वक "
"इसे सुरक्षित तथा अद्यतन रखते हैं तथा सीधे आपको प्रदान करते हैं."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/italc.html:1
msgid "iTalc intelligent teaching"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/italc.html:6
msgid "<em>iTalc</em> allows a lecturer to take control of student desktops."
msgstr ""
"<em>iTalc</em> आपको विद्यार्थी डेक्सटॉप पर नियंत्रण करने के पाठ की अनुमति देता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/italc.html:7
msgid "Broadcast a desktop session to all users to demonstrate a new concept."
msgstr ""
"डेक्सटॉप सत्र के सभी प्रयोक्ता को नए सिद्धान्त का वर्णन करने के लिए प्रसारण की अनुमति देता "
"है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/italc.html:8
msgid "Monitor user sessions to keep tabs on mischievous users."
msgstr "मॉनिटर प्रयोक्ता सत्र शरारती प्रयोक्ता पर टैब रखता है."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/kdeedu.html:1
msgid "Play and learn"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/kdeedu.html:6
msgid ""
"<em>The KDE Education suite</em> includes edutainment software for kids aged "
"3 to 18."
msgstr ""
"<em>केडीई एडूकेशन सुईट</em> में 3 से 18 साल के बच्चो के लिए शिक्षात्मक सॉफ्टवेयर समाहित "
"करता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/kdeedu.html:7
msgid "Improve language skills with letters, anagram and hangman games."
msgstr "भाषा निपुणता को अक्षर, अनाग्राम तथा हैंगमैन खेल के द्वारा निखारें."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/kdeedu.html:8
msgid ""
"Improve mathematical skills with fraction, geometry and algebra software."
msgstr "गणित निपुणता को भिन्न, रेखागणित तथा बीजगणित सॉफ्टवेयर के द्वारा निखारें."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/kdeedu.html:9
msgid ""
"Also included are geography games, turtle programming, a typing tutor, "
"memory exercises and more!"
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/librecad.html:1
msgid "LibreCAD computer aided drawing"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/librecad.html:6
msgid ""
"<em>LibreCAD</em> is a an application for 2D Computer Aided Design (CAD)."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/librecad.html:7
msgid ""
"Create technical drawings such as building plans, interiors or mechanical "
"parts."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/librecad.html:8
msgid "Draw up schemas and diagrams."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/ltsp.html:1
msgid "Easy terminal server setup"
msgstr "आसान टर्मिनल सर्वर विन्यास"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ltsp.html:6
msgid "Edubuntu ships with <em>LTSP</em>, the Linux Terminal Server Project."
msgstr "एडुबन्टू <em>एलटीएसपी</em> (लाइनेक्स टर्मिनल सर्वर परियोजना) के साथ आता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ltsp.html:7
msgid ""
"You can try out a demo by launching LTSP Live from the Applications -> Other "
"menu from within this live environment."
msgstr ""
"आप एक डेमो की जाँच अनुप्रयोग -> अन्य सूची से एलटीएसपी लाइव का लॉन्च लाइव वातावरण के "
"अंतर्गत कर, कर सकते हैं."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ltsp.html:8
msgid ""
"Extend to large-scale deployments with LTSP-Cluster, a high availability "
"clustering suite available for Edubuntu."
msgstr ""
"एडुबन्टू के साथ उपलब्ध एलटीएसपी-क्लस्टर, एक उच्च क्लस्टरिंग सुइट के साथ विस्तृत-पैमाने पर "
"फैलाव करें."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/nanny.html:1
msgid "Gnome Nanny"
msgstr "गनोम नैनी"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/nanny.html:6
msgid "<em>Gnome Nanny</em> provides parental control in Edubuntu."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/nanny.html:7
msgid ""
"Limit the amount of time kids spend on the computer in any period using the "
"graphical schedule editor."
msgstr ""
"कंप्यूटर पर बच्चो द्वारा बिताए गए समय को किसी भी समय सीमा के लिए चित्रादि कार्यक्रम "
"संपादक के द्वारा सीमित कर सकते हैं."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/nanny.html:8
msgid ""
"Restrict access to undesirable web sites and download pre-defined access "
"rules."
msgstr ""
"पूर्व-वर्णित पहुँच नियम द्वारा आपत्तिजनक वेबसाइट तथा डाउनलोड के पहुँच पर रोक लगाएँ."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/pencil.html:1
msgid "Drawing with Pencil"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pencil.html:6
msgid "Edubuntu ships with <em>Pencil</em>, a multi-purpose drawing tool."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pencil.html:7
msgid "Create traditional hand-drawn animations or cartoons."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pencil.html:8
msgid "Mix up bitmap and vector graphics."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pencil.html:9
msgid "Start a comic-strip or draw a self-portrait!"
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/pessulus.html:1
msgid "Pessulus Lockdown Editor"
msgstr "पेसुलस लॉकडाउन संपादक"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pessulus.html:6
msgid ""
"The <em>Pessulus Lockdown Editor</em> allows administrators to restrict "
"users from making certain changes to their profile."
msgstr ""
"<em>पेसुलस लॉकडाउन संपादक</em> प्रशासक को अपने प्रोफाइल में कुछ निश्चित परिवर्तन करने से "
"रोकने की अनुमति देता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pessulus.html:7
msgid "Lock down panels and user settings easily on public machines."
msgstr "सार्वजनिक मशीन पर पटल में ताला लगाना तथा प्रयोक्ता विन्यास आसान है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/pessulus.html:8
msgid "Launch browsers in fullscreen mode in kiosk environments."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/sabayon.html:1
msgid "Sabayon user profile editor"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/sabayon.html:6
msgid ""
"The <em>Sabayon User Profile Editor</em> allows administrators to set up "
"highly customized desktops for users."
msgstr ""
"<em>सबायोन प्रोयक्ता प्रोफाइल संपादक</em> प्रशासक को प्रयोक्ता हेतु उच्च विनिर्मित "
"डेक्सटॉप नियत करने की अनुमति देता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/sabayon.html:7
msgid ""
"Launch the Sabayon Profile Editor from the Administration menu, and you'll "
"be able to set up a desktop just the way you want it."
msgstr ""
"प्रशासन सूची से सबायोन प्रोफाइल संपादक को आरम्भ करें, तथा आप अपनी इच्छानुसार डेक्सटॉप को "
"नियत करने में समर्थ हो जाएगें."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/sabayon.html:8
msgid "Easily apply custom desktops that have been created to users or groups."
msgstr "आसानी के लागू होने वाले मनपसंद डेक्सटॉप जो प्रयोक्ता या समूह के लिए बनाया गया है."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/testdrive.html:1
msgid "Testdrive in Software Center"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testdrive.html:6
msgid ""
"<em>Testdrive</em> allows you to try out software over the web before "
"installation."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testdrive.html:7
msgid ""
"Try an application out before deciding whether it's the right tool for the "
"job."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testdrive.html:8
msgid ""
"Demo applications to users on their machines without making any permanent "
"local changes."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testdrive.html:9
msgid ""
"Find it in Software Center, which can be found under the Applications menu."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/testing.html:1
msgid "Welcome and thank you"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testing.html:6
msgid "Thank you for testing what will become Edubuntu 11.10."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testing.html:7
msgid ""
"If this is an alpha/beta candidate, please log this installation on <a href="
"\"http://iso.qa.ubuntu.com/\">http://iso.wa.ubuntu.com</a>."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testing.html:8
msgid ""
"File bugs at <a href=\"http://bugs.launchpad.net\">http://bugs.launchpad."
"net</a>."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testing.html:9
msgid "Talk to us on irc.freenode.net, channel #edubuntu"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/testing.html:10
msgid ""
"Most up to date news at <a href=\"http://edubuntu.org\">http://edubuntu.org</"
"a>"
msgstr ""

#. type: Content of: <div><div><p>
#: slideshows/edubuntu/slides/testing.html:12
msgid "This slide will be removed after the beta release."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/tux4kids.html:1
msgid "Learning is fun with Tux4kids"
msgstr "टक्स4किड के साथ सिखना मजेदार है"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/tux4kids.html:6
msgid ""
"Defend your cities in <em>Tux of Math Command</em>, an arcade game that "
"improves arithmetic skills ."
msgstr ""
"अपने नगर का बचाव करें <em>टक्स के गणित कमांड</em> में, एक ऐसा ऑर्केड खेल जो अकंगणित कौशल "
"को बढ़ाता है."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/tux4kids.html:7
msgid ""
"Learn basic computer skills and unleash your creativity with <em>TuxPaint</"
"em>."
msgstr ""
"आधारभूत कंप्यूटर कौशल सीखें तथा <em>टक्सपेंट</em> के साथ अपने रचनात्मकता को उन्मुक्त करें."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/tux4kids.html:8
msgid ""
"Learn where all the buttons on your keyboard are using <em>TuxType</em>."
msgstr "<em>टक्सटाइप</em> का उपयोग कर अपने कुंजीपटल के सभी बटन का उपयोग करना सीखें."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/ubuntuone.html:1
msgid "Keep your digital life in sync"
msgstr "अपने डिजीटल जिंदगी को समकालिक रखें"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ubuntuone.html:6
msgid ""
"Shuffle between your laptop, netbook, and company desktop and find "
"everything you need automatically synchronized. Your documents, music, "
"bookmarks, address book, and notes – everything goes where you go with "
"<em>Ubuntu One</em>."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ubuntuone.html:7
msgid ""
"Want a safe home for your important files? All Edubuntu users get 2 GB of "
"personal cloud storage for free (and more if you need it)."
msgstr ""
"अपने आयातित फ़ाइल हेतु सुरक्षित घर चाहते हैं? सभी एडुबन्टू प्रयोक्ता 2 जीबी निजी समूह भंडार "
"मुफ्त में प्राप्त करते हैं (और जरुरत होने पर और ज्यादा)."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ubuntuone.html:8
msgid ""
"Share folders using your synchronized address book. Project collaboration "
"has never been easier."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/ubuntuone.html:9
msgid ""
"Sign-up by launching the <em>Ubuntu One Preferences</em> application or "
"going to <a href=\"http://one.ubuntu.com/\">http://one.ubuntu.com/</a>."
msgstr ""
"साइन-अप कर <em>उबुन्टू एकल वरियता</em> अनुप्रयोग का लॉच करें या जाएँ <a href="
"\"http://one.ubuntu.com/\">http://one.ubuntu.com/</a>."

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/underthehood.html:1
msgid "Under the hood"
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/underthehood.html:6
msgid "Edubuntu is built on the latest and greatest free software available."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/underthehood.html:7
msgid "Linux 3.0 boosts performance and hardware support."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/underthehood.html:8
msgid "Xorg 1.10, Mesa 7.11 and Compiz delivers breathtaking graphics support."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/underthehood.html:9
msgid "Python 2.7 provides increased performance and functionality."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/edubuntu/slides/welcome.html:1
msgid "Welcome"
msgstr "स्वागतम्"

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/welcome.html:6
msgid "Thank you for choosing Edubuntu 11.10."
msgstr ""

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/welcome.html:7
msgid ""
"This release brings updates, bug fixes and new software. Please refer to the "
"release notes on our website for the most up to date information."
msgstr ""
"यह प्रकाशन अद्यतन, दोष सुधार तथा नए सॉफ्टवेयर प्रस्तुत करता है. कृपया अधिकांश समंक सूचना "
"हेतु हमारे वेबसाइट पर प्रकाशन टिप्पणी का संदर्भ ले."

#. type: Content of: <div><div><ul><li>
#: slideshows/edubuntu/slides/welcome.html:8
msgid ""
"Whether you're new to Edubuntu or a long-time user, we're sure there is "
"something you will enjoy. While Edubuntu is installed, this slideshow will "
"show you around."
msgstr ""
"एडुबन्टू के लिए भले ही आप नए हो या पुराने प्रयोक्ता, हमें विश्वास है कि आप इसका आनंद लेंगे. "
"जब एडुबन्टू संस्थापित हो रहा होगा, यह स्लाईडशो आपको दिखाई देगा."

#. type: Content of: <div><div><p>
#: slideshows/edubuntu/slides/welcome.html:10
msgid "Edubuntu is designed to be easy. Feel free to explore!"
msgstr "एडुबन्टू का डिजाइन आसान है. उन्मुक्ता के साथ अन्वेषण करें!"

#~ msgid "Thank you for choosing Edubuntu 11.04."
#~ msgstr "एडुबन्टू 11.04 को चुनने के लिए धन्यवाद."
