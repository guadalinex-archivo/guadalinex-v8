# Finnish translation for ubiquity-slideshow-ubuntu
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the ubiquity-slideshow-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: ubiquity-slideshow-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2011-09-28 16:16+0200\n"
"PO-Revision-Date: 2011-09-25 10:01+0000\n"
"Last-Translator: Sasu Karttunen <sasu.karttunen@tpnet.fi>\n"
"Language-Team: Finnish <fi@li.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-10-03 09:05+0000\n"
"X-Generator: Launchpad (build 14071)\n"

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/accessibility.html:1
msgid "Accessibility in Kubuntu"
msgstr "Kubuntun esteettömyys"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/accessibility.html:6
msgid ""
"We want to make computers work for everyone, whatever your physical "
"circumstances. So, we provide tools that make Kubuntu one of the most "
"accessible operating systems around."
msgstr ""
"Haluamme tietokoneiden olevan kaikkien käytettävissä riippumatta "
"mahdollisista liikuntarajoitteista. Sen vuoksi tarjoammekin työkaluja, jotka "
"tekevät Kubuntusta yhden esteettömimmin käytettävistä käyttöjärjestelmistä."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/accessibility.html:9
msgid ""
"You can get at these tools in one place: the <em>Accessibility</em> "
"preferences, inside the System Settings application from the menu. From "
"there, you can turn on helpful tools like <em>Modifier Keys</em>, "
"<em>Keyboard Filters</em>, and <em>Activation Gestures</em>."
msgstr ""
"Löydät nämä työkalut yhdestä paikasta: Järjestelmän asetuksien alta "
"<em>Esteettömyys</em>-asetuksista. Sieltä on mahdollista ottaa käyttöön "
"käyttöä helpottavia asetuksia kuten <em>muuntonäppäimet</em>, "
"<em>näppäimistösuodattimet</em> ja <em>aktivointieleet</em>."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/accessibility.html:14
msgid ""
"Remember to also look over the <em>Appearance Preferences</em>, too. You can "
"choose between different visual styles and even change the fonts that are "
"used by applications."
msgstr ""
"Muista myös tutustua <em>Sovellusten ulkoasu</em> -asetuksiin. Voit vaihtaa "
"sovellusten ulkoasua vaihtamalla eri tyylien välillä tai vaikkapa "
"muuttamalla sovellusten käyttämiä kirjasimia."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/games.html:1
msgid "Get your game on with Kubuntu"
msgstr "Käy kiinni peleihin Kubuntussa"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/games.html:6
msgid ""
"With hundreds of game titles in our online repository, Kubuntu allows you to "
"not be all about work, but also allows you to play."
msgstr ""
"Tuhannet pakettivarastossamme olevat pelit mahdollistavat myös pelaamisen "
"Kubuntulla työnteon lisäksi."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/games.html:9
msgid ""
"The <em>KDE Software Compilation</em> has quite a few games ranging from "
"card games to logic games and board games."
msgstr ""
"<em>KDE-ohjelmistokokonaisuus</em> sisältää paljon erilaisia pelejä, "
"korttipeleistä logiikkapeleihin."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/games.html:11
msgid ""
"First Person Shooters, Role Play Games, and more are also available in the "
"repositories."
msgstr ""
"Pakettivarastoissa on saatavilla myös FPS-ammuskelupelejä, roolipelejä ja "
"monia muita."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/get-help.html:1
msgid "Getting help with Kubuntu"
msgstr "Apua Kubuntun käyttöön"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-help.html:6
msgid ""
"If you need help, try <em>Help</em> from the menu, or the <em>Help</em> menu "
"in most applications."
msgstr ""
"Jos tarvitset apua Kubuntun käytössä, kokeile erillistä sovellusvalikosta "
"löytyvää <em>Ohje</em>-ohjelmaa tai useimmista ohjelmista löytyvää <em>Ohje</"
"em>-valikkoa."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-help.html:8
msgid ""
"In addition to our extensive written help, the Kubuntu community in "
"conjunction with the Ubuntu community, provides free technical support in "
"person and over the Internet. There is also commercial support available "
"through Canonical, its partners and approved companies. Learn more at <a "
"href=\"http://www.ubuntu.com/support\">ubuntu.com/support</a>."
msgstr ""
"Laajojen kirjoitettujen ohjeiden lisäksi Kubuntun yhteisö tarjoaa ilmaista "
"teknistä tukea henkilökohtaisesti sekä Internetin välityksellä. Canonicalin, "
"sen yhteistökumppaneiden ja hyväksymien yritysten kautta on saatavilla myös "
"kaupallista tukea. Lisätietoja löydät osoitteista <a href=\"http://www."
"ubuntu.com/support\">ubuntu.com/support</a> ja <a href=\"http://wiki.ubuntu-"
"fi.org/Ohjeet\">ubuntu-fi.org</a>."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-help.html:14
msgid ""
"Let us know about your Kubuntu experience at <a href=\"http://www.ubuntu.com/"
"community\">ubuntu.com/community</a>!"
msgstr ""
"Kerro meille Kubuntu-kokemuksistasi osoitteessa <a href=\"http://ubuntu.com/"
"community\">ubuntu.com/community</a>."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/get-involved.html:1
msgid "Get involved and contribute to Kubuntu"
msgstr "Tule mukaan tekemään Kubuntusta entistä parempi"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-involved.html:6
msgid ""
"The Kubuntu community consists of a group of individuals, who make up one of "
"the greatest teams in the open source community, work on various aspects of "
"the distribution, providing advice and technical support, as well as helping "
"to promote Kubuntu to a wider audience."
msgstr ""
"Kubuntu-yhteisöön kuuluu henkilöitä, jotka muodostavat yhden suurimmista "
"ryhmistä avoimen lähdekoodin yhteisössä. He työskentelevät eri asioiden "
"parissa levittäen Kubuntua, antamalla neuvoja sekä teknistä tukea ja "
"esittelemällä Kubuntua yhä laajemmalle yleisölle."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-involved.html:11
msgid ""
"No matter your skill level, getting involved and helping to mold the future "
"of Kubuntu is easy, and you are more than welcome to help out."
msgstr ""
"Taitotasolla ei ole väliä! Mukaan pääseminen ja Kubuntun paremmaksi "
"paremmaksi muokkaaminen on helppoa. Olet enemmän kuin tervetullut auttamaan."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/get-involved.html:14
msgid ""
"Look over <a href=\"https://wiki.kubuntu.org/Kubuntu/GettingInvolved\">wiki."
"kubuntu.org/Kubuntu/GettingInvolved</a> and see where you might be able to "
"help out. Remember, it is easy to get involved and the work you do will be "
"seen by millions of people around the world."
msgstr ""
"Käy sivulla <a href=\"https://wiki.kubuntu.org/Kubuntu/GettingInvolved"
"\">wiki.kubuntu.org/Kubuntu/GettingInvolved</a> ja katso missä voisit "
"auttaa. Muista, että mukaan on helppo päästä ja miljoonat ihmiset ympäri "
"maailmaa näkevät tekemäsi työn."

#. type: Content of: <div><div><p>
#: slideshows/kubuntu/slides/get-involved.html:20
msgid "The installation will finish soon. We hope you enjoy Kubuntu."
msgstr "Asennus valmistuu pian. Toivomme, että nautit Kubuntun käytöstä."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/graphics.html:1
msgid "Organize, enjoy, and share your photos"
msgstr "Järjestele, katsele ja jaa valokuviasi"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/graphics.html:5
msgid ""
"With <em>Gwenview</em>, it is really easy to organize and share your photos."
msgstr ""
"<em>Gwenview</em>-sovelluksella kuvien järjesteleminen ja jakaminen on hyvin "
"helppoa."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/graphics.html:7
msgid ""
"Use the Export option to copy your photos to a remote computer, iPod, a "
"custom HTML gallery, or to export to services such as Flickr, SmugMug, "
"PicasaWeb, and more."
msgstr ""
"Vie-valintaa käyttämällä voit kopioida valokuvia etätietokoneelle, iPodiin, "
"erilliseen HTML-galleriaan tai esimerkiksi Flickr-, SmugMug-, ja PicasaWeb-"
"palveluihin."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/graphics.html:10
msgid ""
"For more advanced organization and editing features, <em>digiKam</em> is "
"available for installation."
msgstr ""
"Saatavilla on myös <em>digiKam</em>-sovellus edistyneempiä järjestely- ja "
"muokkaustoimintoja tarvitseville."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/installing-software.html:1
msgid "Installing additional software"
msgstr "Uusien sovellusten asentaminen"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/installing-software.html:6
msgid ""
"Take a look at <em>Software Management</em> application in the menu under "
"the System tab. Software Management will allow you to install (and remove) "
"software from our online repositories, which we carefully organize to be "
"safe and up to date."
msgstr ""
"Tutustu <em>Ohjelmistonhallinta</em> -sovellukseen valikon Järjestelmä-"
"välilehdessä. Ohjelmistonhallinnan avulla voit asentaa (ja poistaa) "
"ohjelmia, jotka sijaitsevat tarkasti ylläpidetyissä ohjelmistolähteissä."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/installing-software.html:10
msgid ""
"There is great software for everything from creating music and movies to "
"producing 3D models and exploring the universe."
msgstr ""
"Tarjolla on loistavia sovelluksia aina musiikin ja elokuvien luomisesta "
"kolmiulotteisten mallien rakentamiseen sekä maailmankaikkeuden tutkimiseen."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/installing-software.html:12
msgid ""
"If you need something that isn't available through us, find out if there is "
"a Debian package or another repository available. That way, it will be "
"really easy to install and you will receive automatic updates."
msgstr ""
"Jos jotain sovellusta ei ole tarjolla, selvitä tarjoaako kolmas osapuoli "
"Debian-pakettia tai vaihtoehtoista ohjelmalähdettä. Tällöin sovellusten "
"asentaminen on helppoa ja päivitykset vastaanotetaan automaattisesti."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/internet.html:1
msgid "The Internet, your way"
msgstr "Internet sinun tavallasi"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/internet.html:5
msgid ""
"Kubuntu ships software necessary to utilize the Internet for many different "
"situations."
msgstr "Kubuntun mukana tulee ohjelmistot Internetin käyttöön eri tilanteissa."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/internet.html:7
msgid ""
"Browse the web, share your files, software, and multimedia, send and receive "
"e-mail, and communicate with friends and family."
msgstr ""
"Selaa WWW-sivuja, jaa tiedostojasi, vastaanota sekä lähetä sähköposteja ja "
"pidä yhteyttä tuttaviisi sekä perheeseesi."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/internet.html:10
msgid ""
"Web browsers such as <em>Firefox</em> and Google's <em>Chromium</em> are "
"easily installable."
msgstr ""
"<em>Firefox</em>- ja Googlen <em>Chromium</em>-selaimet ovat myös helposti "
"asennettavissa."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/multimedia.html:1
msgid "Music and movies in Kubuntu"
msgstr "Musiikki ja elokuvat Kubuntussa"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/multimedia.html:5
msgid ""
"Kubuntu is ready to play videos and music from the web, from CDs and DVDs."
msgstr ""
"Kubuntulla on mahdollista toistaa videoita ja musiikkia Internetistä sekä "
"CD- ja DVD-levyiltä."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/multimedia.html:7
msgid ""
"<em>Amarok</em> audio player lets you organize your music and listen to "
"Internet radio, podcasts, and more, as well as synchronizes your audio "
"collection to a portable audio player."
msgstr ""
"<em>Amarok</em>-musiikkisoittimen avulla voit järjestellä "
"musiikkikokoelmaasi, kuunnella Internet-radiota ja podcasteja, sekä "
"synkronoida musiikkisi kannettavalle musiikkisoittimelle."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/multimedia.html:10
msgid ""
"<em>Dragon Player</em> allows you to easily watch videos from your computer, "
"DVD, or streamed over the Internet."
msgstr ""
"<em>Dragon-soitin</em> mahdollistaa videoiden katselun tietokoneelta, DVD-"
"levyiltä sekä Internetistä."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/office.html:1
msgid "Office tools at your fingertips"
msgstr "Toimistotyökalut käden ulottuvillasi"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/office.html:5
msgid ""
"<em>LibreOffice</em> is a powerful office software suite that is very easy "
"to learn and use."
msgstr ""
"<em>LibreOffice</em> on tehokas toimisto-ohjelmisto, jota on hyvin helppo "
"oppia käyttämään."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/office.html:7
msgid ""
"Use it to create letters, presentations and spreadsheets, as well as "
"diagrams and databases."
msgstr ""
"Se on kätevä kirjeiden, esitysten, taulukoiden sekä piirrosten ja "
"tietokantojen luomiseen."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/office.html:9
msgid ""
"<em>LibreOffice</em> works with documents from other popular office "
"applications include WordPerfect and Microsoft Office. It uses the standard "
"OpenDocument format."
msgstr ""
"<em>LibreOffice</em> toimii yhteen kaikkien suosittujen toimistosovellusten "
"kanssa, mukaan lukien WordPerfect ja Microsoft Office. Oletuksena se käyttää "
"OpenDocument-muotoa, joka on ISO-standardi."

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/pim.html:1
msgid "Manage your contacts, dates, and e-mail"
msgstr "Hallitse yhteystietoja, tapaamisia ja sähköpostia"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/pim.html:6
msgid ""
"<em>Kontact</em> is the fully-featured personal information management "
"software that comes with Kubuntu. It contains applications for E-mail, "
"Calendar, Address Book, and more."
msgstr ""
"Kubuntun mukana tuleva <em>Kontact</em> on henkilökohtainen "
"tiedonhallintaohjelmisto. Se sisältää sähköpostin, kalenterin, osoitekirjan "
"ja paljon muuta."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/pim.html:9
msgid ""
"Send e-mail with <em>KMail</em> from services such as Yahoo, GMail, and "
"various groupware services."
msgstr ""
"Lähetä sähköposteja Yahoo Maililla, GMaililla sekä useilla "
"työryhmäpalveluilla käyttäen <em>KMail</em>-sovellusta."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/pim.html:11
msgid ""
"Organize your calendar and scheduling with <em>KOrganizer</em> and "
"synchronize with services such as Google's Calendar."
msgstr ""
"Järjestele kalenterisi ja aikataulusi sekä synkronoi ne esimerkiksi Google-"
"kalenterin kanssa <em>KOrganizer</em>illa."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/pim.html:13
msgid ""
"Keep your contacts organized with <em>KAddressBook</em> and import or export "
"your contacts with nearly every address book standard as well synchronize "
"with various groupware services."
msgstr ""
"Pidä yhteystietosi järjestyksessä <em>KAddressBook</em>-sovelluksen avulla. "
"Tuo tai vie yhteystietosi lähes kaikkien osoitekirjastandardien muodossa. "
"Voit myös synkronoida tiedot monien ryhmätyöpalvelujen kanssa."

#. type: Content of: <div><div><p>
#: slideshows/kubuntu/slides/pim.html:17
msgid ""
"<em>Kontact</em> is the perfect personal information management integration "
"for you!"
msgstr ""
"<em>Kontact</em> on täydellinen henkilökohtaisen tiedon hallintaohjelma "
"sinulle!"

#. type: Content of: <h1>
#: slideshows/kubuntu/slides/welcome.html:1
msgid "Welcome"
msgstr "Tervetuloa"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/welcome.html:6
msgid "Thank you for choosing Kubuntu 11.10."
msgstr "Kiitos kun valitsit Kubuntu 11.10:n"

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/welcome.html:7
msgid ""
"We believe every computer user should be free to work in the environment of "
"their choice and to download, change, study and share their software for any "
"purpose."
msgstr ""
"Uskomme, että jokaisella tietokoneen käyttäjällä tulee olla oikeus "
"työskennellä haluamassaan ympäristössä, ja että heillä tulee olla vapaudet "
"noutaa, muuttaa, tutkia ja jakaa kyseisiä ohjelmistoja mihin tahansa "
"käyttöön."

#. type: Content of: <div><div><ul><li>
#: slideshows/kubuntu/slides/welcome.html:10
msgid ""
"As part of our promise, we want Kubuntu to work as well for you as possible. "
"So while it installs, this slideshow will provide a quick introduction."
msgstr ""
"Osana lupaustamme haluamme Kubuntun toimivan mahdollisimman hyvin myös sinun "
"käytössäsi. Asennuksen aikana tämä esitys tarjoaa pikaisen johdannon "
"Kubuntuun."

#. type: Content of: <div><div><p>
#: slideshows/kubuntu/slides/welcome.html:14
msgid "Kubuntu is designed to be easy. Feel free to explore!"
msgstr ""
"Kubuntu on suunniteltu ja rakennettu helppokäyttöisyyttä silmällä pitäen. "
"Tutki vapaasti!"

#~ msgid "Thank you for choosing Kubuntu 11.04."
#~ msgstr "Kiitos Kubuntu 11.04:n valinnasta."
