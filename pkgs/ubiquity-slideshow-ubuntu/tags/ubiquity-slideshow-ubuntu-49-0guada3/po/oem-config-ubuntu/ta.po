# Tamil translation for ubiquity-slideshow-ubuntu
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the ubiquity-slideshow-ubuntu package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: ubiquity-slideshow-ubuntu\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2011-09-28 16:17+0200\n"
"PO-Revision-Date: 2011-02-22 22:22+0000\n"
"Last-Translator: Mel C <melwin.a3@gmail.com>\n"
"Language-Team: Tamil <ta@li.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-10-03 09:06+0000\n"
"X-Generator: Launchpad (build 14071)\n"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:1
msgid "Customise Ubuntu"
msgstr "விருப்பப்படி Ubuntuவை மாற்ற"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:8
msgid ""
"At the heart of Ubuntu’s philosophy is a belief that computing is for "
"everyone. With advanced accessibility tools and options like your preferred "
"color scheme, text size, and language, Ubuntu is for anyone. Whoever you "
"are, and wherever you are."
msgstr ""

#. type: Content of: <div><div><div><h2>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:12
msgid "Customisation options"
msgstr "விரும்பியபடி மாற்ற"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:16
msgid "Appearance"
msgstr ""

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:20
msgid "Assistive technologies"
msgstr "மாற்றுத்திறனாளிக்கு உதவும் தொழில்நூட்பங்கள்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/accessibility.html:24
msgid "Language support"
msgstr "ஆதரிப்பு மொழிகள்"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/browse.html:1
msgid "Browse the web"
msgstr "இணையத்தில் உலா வர"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/browse.html:8
msgid ""
"Ubuntu includes Mozilla Firefox for fast, safe web browsing. You can also "
"choose alternative browsers from the Ubuntu Software Center."
msgstr ""

#. type: Content of: <div><div><div><h2>
#: slideshows/oem-config-ubuntu/slides/browse.html:12
#: slideshows/oem-config-ubuntu/slides/music.html:12
#: slideshows/oem-config-ubuntu/slides/office.html:12
#: slideshows/oem-config-ubuntu/slides/photos.html:12
msgid "Included software"
msgstr "இணைக்கப்பட்ட மென்பொருள்கள்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/browse.html:16
msgid "Firefox web browser"
msgstr "பயர்பாக்ஸ் இணைய உலாவி"

#. type: Content of: <div><div><div><h2>
#: slideshows/oem-config-ubuntu/slides/browse.html:19
#: slideshows/oem-config-ubuntu/slides/photos.html:19
msgid "Supported software"
msgstr "மேலும் நீங்கள் விரும்பினால்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/browse.html:23
msgid "Flash"
msgstr "ஃப்ளாஷ்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/browse.html:27
msgid "Chromium"
msgstr "குரோமியம்"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/gethelp.html:1
msgid "Any questions?"
msgstr ""

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/gethelp.html:8
msgid ""
"Check out <a href=\"http://askubuntu.com/\">Ask Ubuntu</a>, the best place "
"to get an answer about Ubuntu. With most questions already answered, and "
"thousands of people ready to help, you’ll be sorted in no time at all. And "
"if you need an answer on a deadline, Canonical offers <a href=\"http://www."
"ubuntu.com/support\">commercial support</a> to business users. Browse to <a "
"href=\"http://www.ubuntu.com/support\">ubuntu.com/support</a> to explore "
"your other options."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/mobilise.html:1
msgid "Your own personal cloud"
msgstr ""

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/mobilise.html:8
msgid ""
"An <a href=\"https://one.ubuntu.com/services/\">Ubuntu One Free</a> account "
"gives you 5GB of cloud storage, so you can store and sync your files and "
"photos across devices and access them wherever you are in the world. Easily "
"share them with friends, family and colleagues. Take a photo on your mobile "
"phone and immediately see it on your desktop, or add <a href=\"https://one."
"ubuntu.com/services/music/\">Music Streaming</a> for mobile to enjoy your "
"music on the move."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/music.html:1
msgid "Take your music with you"
msgstr "உங்கள் பாடல்களைத் தேர்ந்தெடுங்கள்"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/music.html:8
msgid ""
"Ubuntu comes with the amazing Banshee music player. With advanced playback "
"options and the Ubuntu One music store built in, it’s simple to queue up the "
"perfect songs. And it works great with CDs and portable music players, so "
"you can enjoy all your music wherever you go."
msgstr ""

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/music.html:16
msgid "Banshee Music Player"
msgstr "Banshee இசையியக்கி"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/music.html:20
msgid "Ubuntu One Music Store"
msgstr "உபுண்டு ஒன் இசை அங்காடி"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/office.html:1
msgid "Write and present for free"
msgstr ""

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/office.html:8
msgid ""
"LibreOffice is a free office suite packed with everything you’ll need to "
"create impressive documents, spreadsheets, and presentations. LibreOffice "
"tries its best to work with other office software, and it uses the "
"OpenDocument standards for far-reaching compatibility."
msgstr ""

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/office.html:16
msgid "LibreOffice"
msgstr "லிபரேஆபீஸ்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/office.html:20
msgid "Tomboy Notes"
msgstr "டாம்பாய் குறிப்புகள்"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/photos.html:1
msgid "Have fun with your photos"
msgstr "புகைப்படங்களுடன் புகுந்து விளையாட"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/photos.html:8
msgid ""
"Shotwell is a handy photo manager that is ready for your gadgets. Connect a "
"camera or a phone to transfer your photos, then it’s easy to share them and "
"keep them safe. If you’re feeling creative, you can try lots of photo apps "
"from the Ubuntu Software Center."
msgstr ""

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/photos.html:16
msgid "Shotwell Photo Manager"
msgstr "ஷாட்வெல் புகைப்படத் தொகுப்பி"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/photos.html:23
msgid "GIMP Image Editor"
msgstr "ஜிம்ப் புகைப்பட தொகுப்பான்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/photos.html:27
msgid "Pitivi Video Editor"
msgstr "பிட்வி காணொளி தொகுப்பி"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/social.html:1
msgid "Stay connected"
msgstr ""

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/social.html:8
msgid ""
"The Ubuntu Message Indicator gives you an eagle-eye view of incoming "
"messages from all applications and social networks. See at a glance when "
"there’s something new to read, regardless how it arrived."
msgstr ""

#. type: Content of: <div><div><div><h2>
#: slideshows/oem-config-ubuntu/slides/social.html:12
msgid "Supported services"
msgstr "ஓத்திசைவு சேவைகள்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/social.html:16
msgid "Twitter"
msgstr "ட்விட்டர்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/social.html:20
msgid "Facebook"
msgstr "பேஸ்புக்"

#. type: Content of: <div><div><div><ul><li><p>
#: slideshows/oem-config-ubuntu/slides/social.html:24
msgid "Identi.ca"
msgstr "Identi.ca"

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/usc.html:1
msgid "Find even more software"
msgstr "மேலும் தேவையான மென்பொருள்கள்"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/usc.html:8
msgid ""
"The Ubuntu Software Center has thousands of new applications ready for your "
"computer. Just type what you want or explore the categories like games, "
"science, and education. It is easy to download new stuff and write reviews "
"to share your experiences."
msgstr ""

#. type: Content of: <div><h1>
#: slideshows/oem-config-ubuntu/slides/welcome.html:1
msgid "Welcome"
msgstr "நல்வரவு"

#. type: Content of: <div><div><div><p>
#: slideshows/oem-config-ubuntu/slides/welcome.html:8
msgid ""
"Thank you for choosing Ubuntu 11.10. This version brings some exciting "
"changes including a totally redesigned desktop interface, Unity. While "
"Ubuntu is configured, this slideshow will show you around."
msgstr ""

#~ msgid "Appearance preferences"
#~ msgstr "திரையலங்கரம் செய்ய"

#~ msgid ""
#~ "Ubuntu includes Mozilla Firefox for fast, safe web browsing. You can also "
#~ "choose alternative browsers from Ubuntu Software Center."
#~ msgstr ""
#~ "நீங்கள் பாதுகாப்பாக உலாவிட  தன்னிகரற்ற இணைய உலாவியான மொசில்லா பயர்பாக்ஸை "
#~ "இணைத்துள்ளது.விரும்பினால் வேறு  இணைய உலாவிகளை Ubuntu மென்பொருள் அங்காடியில் "
#~ "இருந்து தோர்வு செய்யலாம்."

#~ msgid "Email and chat"
#~ msgstr "மின்னஞ்சல் மற்றும் மின்னரட்டை"

#~ msgid "Evolution Mail"
#~ msgstr "எவல்யூஷன் மின்னஞ்சல்"

#~ msgid "Skype"
#~ msgstr "ஸ்கைபி"

#~ msgid "Social from the start"
#~ msgstr "இணைய சமுதாயத்துடன் இணைய"

#~ msgid ""
#~ "Thank you for choosing Ubuntu 11.04. This version brings some exciting "
#~ "changes including a totally redesigned desktop interface, Unity. While "
#~ "Ubuntu is configured, this slideshow will show you around."
#~ msgstr ""
#~ "உபுண்டு 11,04 தேர்ந்தெடுத்தமைக்கு நன்றி. இந்த பதிப்பு முற்றிலும் மறுவடிவமைக்கப்பட்ட "
#~ "டெஸ்க்டாப் இடைமுகம், ஐக்கிய உட்பட சில அற்புதமான மாற்றங்களை அளிக்கிறது. உபுண்டுவில் "
#~ "அமைக்கப்படும் போது, இந்த ஸ்லைடுஷோ உங்களை சுற்றி காட்டும்."

#~ msgid ""
#~ "At the heart of Ubuntu&#8217;s philosophy is a belief that computing is "
#~ "for everyone. With advanced accessibility tools and many options like "
#~ "your preferred fonts, colour schemes, and languages, Ubuntu is all about "
#~ "working for real people."
#~ msgstr ""
#~ "Ubuntuவின் முதற்கொள்கை கணிணி அனைவருக்கும் உரியதாகும்.எல்லா தரப்பு பயனாளர்களும் "
#~ "பயன்படுத்த எளியதாகயிருக்க உருவாக்கப்பட்டுள்ளது.உங்கள் மனதிற்கு பிடித்த எழுத்துருக்கள்,"
#~ "வண்ணங்கள்,மற்றும் மொழிகளை சுலபமாக தேர்ந்துயெடுக்களாம்."

#~ msgid ""
#~ "The Messaging Menu has lots of features to make online communication "
#~ "effortless. Chat helps you log in to all your instant messaging accounts "
#~ "including MSN, Jabber, and Facebook. Your contacts appear in the same "
#~ "place, ready for text or video chat."
#~ msgstr ""
#~ "தகவல் நிரலி செய்திகளைப் உடனுக்குடன் பரிமாற்றம் செய்யும்.இது பலதரப்பு மின்னரட்டை "
#~ "முகவரிகளை ஆதரிப்பு இதில் உண்டு. ஓலி,ஓளி உரையாடல் வசதிகள் உங்களுக்காக "
#~ "இணைக்கப்பட்டுள்ளது"

#~ msgid "Thank you"
#~ msgstr "நன்றி"

#~ msgid ""
#~ "Thanks again for choosing Ubuntu. Ubuntu is designed to be easy and safe, "
#~ "so don&#8217;t hesitate to explore! If you have any questions, try the "
#~ "Help menu in most applications or browse to <a href=\"http://www.ubuntu."
#~ "com/support\">ubuntu.com/support</a> to explore your other options. We "
#~ "hope you enjoy Ubuntu."
#~ msgstr ""
#~ "Ubuntuவைத் தேர்ந்துயெடுத்தமைக்கு வாழ்த்துக்கள்.கட்டற்ற,அகம்திறந்த உலகில் உலாவுங்கள். "
#~ "ஏதேனும் தயக்கங்கள் இருப்பின்  <a href=\\\"http://www.ubuntu.com/support\\"
#~ "\">ubuntu.com/support</a> தளத்தை அனுகவும்."

#~ msgid "Empathy IM"
#~ msgstr "எம்பத்தி மின்னரட்டை"

#~ msgid ""
#~ "LibreOffice is a free office suite packed with everything you&#8217;ll "
#~ "need to create impressive documents, spreadsheets, and presentations. "
#~ "LibreOffice tries its best to work with other office software, and it "
#~ "uses the OpenDocument standards for far-reaching compatibility."
#~ msgstr ""
#~ "LibreOffice ஒரு கட்டற்ற அலுவலகப்பயன்பாட்டு மென்பொருளாகும்.இது நேர்த்தியான ஆவணங்கள்,"
#~ "பிழையற்ற அட்டவனைகள்,அழகான தொகுப்புகள் உருவாக்க உதவுகிறது.இது இதர மென்பொருகளுடன் "
#~ "ஒத்திசைவு உடையது."

#~ msgid "Mobilise your digital life"
#~ msgstr "மின்னணு உலகில் சிறகடிக்க"

#~ msgid ""
#~ "All Ubuntu users get a free <a href=\"https://one.ubuntu.com/\">Ubuntu "
#~ "One</a> account. Ubuntu One allows you to sync all kinds of files online "
#~ "so you can access them anywhere. Sync bookmarks, contacts, music, and "
#~ "pictures across all your computers. Take everything everywhere with "
#~ "Ubuntu One."
#~ msgstr ""
#~ "எல்லா Ubuntu  பயனீட்டாளர்கள் <a href=\\\"https://one.ubuntu.com/\\\">Ubuntu "
#~ "One</a> என்ற சேவையை இலவசமாய் பெறுவீர்கள்.இது உங்களுக்குத் தேவையான தகவல்களை சேமித்து "
#~ "எங்கு வேண்டுமானாலும் பயன்படுத்த உதவிடும். உங்களுடைய புகைப்படங்கள்,புத்தகக்குறிப்புகள் "
#~ "என எந்தவொரு ஆவணங்களையும் எந்தவொரு கணிணியிலும் பார்க்க உதவுகிறது"

#~ msgid ""
#~ "Ubuntu comes with the amazing Banshee music player. With advanced "
#~ "playback options and the Ubuntu One music store built in, it&#8217;s "
#~ "simple to queue up the perfect songs. And it works great with CDs and "
#~ "portable music players, so you can enjoy all your music wherever you go."
#~ msgstr ""
#~ "Ubuntuவுடன் ஓர் ஒப்பற்ற இசையியக்கி Banshee இணைக்கப்பட்டுள்ளது.இத்துடன் இணைக்கப்பட்ட "
#~ "Ubuntu One இசையங்காடி பாடல்களை வாங்க உதவுகிறது.எல்லா வகையான குறுந்தகடுகள்,"
#~ "பலதரப்பு கையடக்க இசைப்பெட்டிகள் ஆதரிப்பு இதில் உண்டு."

#~ msgid "Write and present with ease"
#~ msgstr "எளிதாக அழகாக எழுத"

#~ msgid ""
#~ "Shotwell is a handy photo manager that is ready for your gadgets. Connect "
#~ "a camera or a phone to transfer your photos, then it&#8217;s easy to "
#~ "share them and keep them safe. If you&#8217;re feeling creative, you can "
#~ "try lots of photo apps from the Ubuntu Software Centre."
#~ msgstr ""
#~ "Shotwell புகைப்படத்தொகுப்பி உங்கள் கேமாரவை அல்லது கைப்பேசியை இணைத்து படங்களைப் "
#~ "பதிவிறக்கம் செய்யவும்,பகிர்வு செய்யவும் உதவுகிறது. மேலும் பல்வேறு புகைப்படம் சார்ந்த "
#~ "மென்பொருள்களை Ubuntu மென்பொருள் அங்காடியில் இருந்து  பதிவிறக்கம் செய்யலாம்."

#~ msgid ""
#~ "The Me Menu is your instant connection to web services like Facebook and "
#~ "Twitter. Just set up your accounts to log in and share with friends. "
#~ "Anything sent to you appears in the Messaging Menu so you can reply when "
#~ "you want."
#~ msgstr ""
#~ "Me Menu உங்கள் திரையிலிருந்து உங்களின் நிலைபாடுகளைக் குறித்த பலுக்கல் செய்திகளைப் "
#~ "பகிர,உங்கள் நண்பர்களின் பலுக்களை அறியவும்,பதில் அளிக்கவும் செய்யலாம்."

#~ msgid "Some of our favourites"
#~ msgstr "எங்களின் சிறந்த தேர்வுகள்"

#~ msgid ""
#~ "The Ubuntu Software Centre has thousands of new applications ready for "
#~ "your computer. Just type what you want or explore the categories like "
#~ "games, science, and education. It is simple to download new stuff and "
#~ "write reviews to share your experiences."
#~ msgstr ""
#~ "Ubuntu மென்பொருள் அங்காடி - உங்களுக்காக ஆயிரக்கணக்கான கட்டற்ற,அகம்திறந்த "
#~ "மென்பொருள்களை வரிசைப்படுத்தி எளிதாக பயன்படுத்துமாறு வடிவமைக்கப்பட்டுள்ளது."

#~ msgid "Inkscape"
#~ msgstr "Inkscape"

#~ msgid "Stellarium"
#~ msgstr "Stellarium"

#~ msgid "Frozen Bubble"
#~ msgstr "Frozen Bubble"
